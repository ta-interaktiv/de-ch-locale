# Swiss German Locale Definition

The locale definition for use with d3-format (number formatting) and d3-time-format (date and time formatting).

## Usage

### React (ES2015) and D3.js

~~~js
// Import
import {timeFormatDefaultLocale} from 'd3-time-format';
import {formatDefaultLocale} from 'd3-format';

const localeDefinition = require('json!ta-de-ch-locale');

// Create Locale
const deChTimeLocale = timeFormatDefaultLocale(localeDefinition);
const deChNumberLocale = formatDefaultLocale(localeDefinition);

// Create Formats!
const percentFormat = deChNumberLocale.format('%');
// etc.
~~~

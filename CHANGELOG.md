# Change Log
Versionierung basiert auf [Semantic Versioning](http://semver.org/).
Changelog-Format basiert auf [Keep A Changelog](http://keepachangelog.com/en/0.3.0/)

## [Unreleased]
Locale definition sourced from https://github.com/d3/d3-time-format/blob/master/locale/de-CH.json and https://github.com/d3/d3-format/blob/master/locale/de-CH.json
